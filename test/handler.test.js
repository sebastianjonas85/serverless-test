var assert = require("assert");
let underTest = require("../handler");
let expectedMessage = 'Go Serverless v1.0! Your function executed successfully!';
let inputObject = { someKey: "someValue" };

describe("handler", () => {
    var promiseUnderTest = underTest.hello(inputObject);

    it("should return statuscode 200", async () => {
        const response = await promiseUnderTest;
        assert.equal(response.statusCode, 200);
    });

    it(`should return "${expectedMessage}" as message`, async () => {
        const response = await promiseUnderTest;
        var body = JSON.parse(response.body);
        assert.equal(body.message, expectedMessage);
    });

    it(`should return "${JSON.stringify(inputObject)}" as input property`, async () => {
        const response = await promiseUnderTest;
        var body = JSON.parse(response.body);
        assert.equal(body.input.someKey, inputObject.someKey);
    });
})